<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("button").click(function(){
    $("p").toggle();
  });
});
</script>
<table border="1" style="width:50%">
<tr><th colspan="8"> Spring </th> </tr>
<tr>
    <th>Issue ID</th>
    <th>Time Spent (minutes)</th>
	<th>Commit Link</th>
    <th>Files that changed </th>
	<th>Number of Files </th>
	<th>Number of extensions</th>
	<th>Heterogeneity Proxy</th>
	<th>Longest Common Prefix</th>
  </tr>
<?php
include("sqlFunctions.php");
include("parsingDataFunction.php");

//take keys from file of git console
$array = readTxtFileOfGithub("sprLog.txt");

$xmlDoc = new DOMDocument();
$xmlDoc -> load("Spring.xml");
$root =  $xmlDoc->getElementsByTagName( "item" ); 
$counter = 0;
$githubRepoLink = array("DATACMNS" => "https://api.github.com/repos/spring-projects/spring-data-commons/commits/","DATAJPA" => "https://api.github.com/repos/spring-projects/spring-data-jpa/commits/","LDAP" => "https://api.github.com/repos/spring-projects/spring-ldap/commits/","BATCH" => "https://api.github.com/repos/spring-projects/spring-batch/commits/","IDE" => "https://api.github.com/repos/spring-projects/spring-ide/commits/","DATAMONGO" => "https://api.github.com/repos/spring-projects/spring-data-mongodb/commits/","INT" => "https://api.github.com/repos/spring-projects/spring-integration/commits/","SWF"=> "https://api.github.com/repos/spring-projects/spring-webflow/commits/","SPR"=> "https://api.github.com/repos/spring-projects/spring-framework/commits/",);
foreach($root As $item){

	 $title = $item->getElementsByTagName( "key" );
	 $title = $title->item(0)->nodeValue; 
	 
	 $xmlKey = $item->getElementsByTagName( "key" );
	 $xmlKey = $xmlKey->item(0)->nodeValue; 
	 
	 $link = $item->getElementsByTagName( "self" );
	 $link = $link->item(0)->nodeValue; 
	 
	 $timespent = $item->getElementsByTagName( "timespent" );
	 $timespent = ($timespent->item(0)->nodeValue) / 60; //convert seconds to minutes
	 set_time_limit(400);
	// foreach($array as $keyInFile => $commitID){
	
	              /* if(preg_match("/\b$xmlKey\b/", $keyInFile))
			          {
						$getNameofProjectKey = multiexplode(array("-"),$xmlKey);
						$githubLink = $getNameofProjectKey[0];
						//echo $xmlKey . "<br />";
						//echo $githubLink . "<br />";
						echo "<tr><td><a href='" . $link . "'>" . $xmlKey . "</a></td><td>" . $timespent . "</td><td><a href='" . $githubRepoLink[$githubLink] . $commitID . "'>Commit Link </a></td></tr>";
						$counter ++;
					  }*/
					  
					  /* if(preg_match("/\b$xmlKey\b/", $keyInFile))
						{
							$getNameofProjectKey = multiexplode(array("-"),$xmlKey);//gets the name of KEY e.g. if SPRING-123 then the variable would be SPRING
							$githubLink = $getNameofProjectKey[0];
							$commitLink = $githubRepoLink[$githubLink] . trim($commitID); //trim removes white space
							$jsonResults = getJsonFromUrl($commitLink);
							$printedResults = printJsonResults($jsonResults);
							
							insertFiles($xmlKey, $printedResults);
							$conn = connectToDB("localhost", "root" , "", "keysandfilechanges");
						    $sqlIssuesAndCommits = "INSERT INTO `issuesandcommits` (`Key`,`KeyLink`,`TimeEffort`,`CommitLink`) VALUES ('$xmlKey', '$link','$timespent','$commitLink')";
							$conn->query($sqlIssuesAndCommits);
							$conn->close();
							//echo "<tr><td><a href='" . $link . "'>" . $xmlKey . "</a></td><td>" . $timespent . "</td><td><a href='" . $commitLink . "'>Commit Link </a></td><td><button>show or hide</button><p>". implode(" ",$printedResults) ."</p></td></tr>";
							$counter ++;
							
						}*/
						
						$conn = connectToDB("localhost", "root" , "", "keysandfilechanges");
						$sql = "SELECT Files FROM `keyandfile` WHERE `Key`='$xmlKey'";
						$result = $conn->query($sql);
						$filesChanged = array();
						if ($result->num_rows > 0) {
							// output data of each row
							while($row = $result->fetch_assoc()) {
							$filesChanged[$xmlKey][] = $row["Files"];
							
							}
						}
						
						$sql2 = "SELECT * FROM `issuesandcommits` WHERE `Key`='$xmlKey'";
						$result2 = $conn->query($sql2);
						if ($result2->num_rows > 0) {
							// output data of each row
							while($row = $result2->fetch_assoc()) {
							if(array_key_exists($xmlKey, $filesChanged)){
							$numberOfExtensions = countFileExtension($filesChanged[$xmlKey]);
							$heterogeneity = calculateHeterogeneity($numberOfExtensions, $filesChanged, $xmlKey);
							/*$jsonResults = getJsonFromUrl($row['CommitLink']);
							$additions = $jsonResults->stats->additions;
							$deletions = $jsonResults->stats->deletions;
							$totAddDel = $jsonResults->stats->total;*/
							//$sql_insert = "INSERT INTO `dataset_heterogeneity_1/ex_x_totfiles` (`Key`,`ProjectName`,`TimeEffort`,`Heterogeneity`) VALUES ('" . $row['Key'] . "', 'Spring', '$timespent', $heterogeneity)";
							//$sql_insert = "INSERT INTO `dataset_numberoffileextensions_and_timespent` (`Project`,`NumberOfFileExtension`,`TimeSpent`) VALUES ('Spring', '" . sizeof($numberOfExtensions) . "', '$timespent')";
							//$sql_insert = "INSERT INTO `dataset_nroffiles_timespent_extensionnr` (`ProjectName`,`NumberOfFiles`,`NumberOfExtensions`,`TimeSpent`) VALUES ('Spring', '" . sizeof($filesChanged[$xmlKey]) . "','" . sizeof($numberOfExtensions) . "', '$timespent')";
							//$sql_insert = "INSERT INTO `dataset_component_longestprefix` (`ProjectName`,`longestPrefix`,`TimeSpent`,`NrOfFiles`) VALUES ('Spring', '" . longestCommonPrefix($filesChanged[$xmlKey]) . "','$timespent', '" . sizeof($filesChanged[$xmlKey]) . "')";
 							//$sql_insert = "INSERT INTO `dataset_addition_deletion` (`ProjectName`,`Key`,`TimeSpent`,`Additions`,`Deletions`,`TotalAddDel`,`NrOfFiles`,`NrOfExtensions`) VALUES ('Spring', '$xmlKey', '$timespent', '$additions', '$deletions', '$totAddDel', '" . sizeof($filesChanged[$xmlKey]) . "', '" . sizeof($numberOfExtensions) . "')";
                           
							$sql_insert = "UPDATE `proxys` SET `LonegstCommonPrefix` = " . longestCommonPrefix($filesChanged[$xmlKey]) . " WHERE `Key` = '" . $row['Key'] . "'";
							$conn->query($sql_insert);
							echo "<tr><td><a href='" . $row['KeyLink'] . "'>" . $row['Key'] . "</a></td><td>" . $row['TimeEffort'] . "</td><td><a href='" . $row['CommitLink'] . "'> " . $row['CommitLink'] . " </a></td><td><button>show or hide</button><p>" . implode(" <br />",$filesChanged[$xmlKey]) . "</p></td><td>". sizeof($filesChanged[$xmlKey]) ."</td><td>" . http_build_query($numberOfExtensions, ' ', '<br />') . "</td><td>". $heterogeneity . "</td><td>" . longestCommonPrefix($filesChanged[$xmlKey]) . " </td></tr>";
							$counter ++;
							}}
						}
						$conn->close(); 
				
			
		//}
}
echo " <b> Resutls:" . $counter . "</b>";
echo calculateAverageOfStringLengthFor("Spring");





?>

</table>