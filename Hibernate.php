<!DOCTYPE html>
<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("button").click(function(){
    $("p").toggle();
  });
});
</script>
</head>
<table border="1" style="width:50%">
<tr><th colspan="8"> Hibernate ORM </th> </tr>
<tr>
    <th>Issue ID</th>
    <th>Time Spent (minutes)</th>
	<th>Commit Link</th>
	<th>Files that changed </th>
	<th>Number of Files </th>
	<th>Number of extensions</th>
	<th>Heterogeneity Proxy</th>
	<th>Longest Common Prefix</th>
  </tr>
<?php

include("sqlFunctions.php");
include("parsingDataFunction.php");


//take keys from file of git console
$array = readTxtFileOfGithub("logi.txt");
$stringLength = array();


$xmlDoc = new DOMDocument();
$xmlDoc -> load("Hibernate.xml");
$root =  $xmlDoc->getElementsByTagName( "item" ); 
$counter = 0;
foreach($root As $item){

	 $title = $item->getElementsByTagName( "title" );
	 $title = $title->item(0)->nodeValue; 
	 
	 $xmlKey = $item->getElementsByTagName( "key" );
	 $xmlKey = $xmlKey->item(0)->nodeValue; 
	 
	 $link = $item->getElementsByTagName( "link" );
	 $link = $link->item(0)->nodeValue; 
	 
	 $timespent = $item->getElementsByTagName( "timespent" );
	 $timespent = ($timespent->item(0)->getAttribute('seconds')) / 60 ; //convert seconds to minutes
	 set_time_limit(2000);
	 //foreach($array as $keyInFile => $commitID){
	
	              /* if(preg_match("/\b$xmlKey\b/", $keyInFile))
			          {
			    echo "<tr><td><a href='" . $link . "'>" . $xmlKey . "</a></td><td>" . $timespent . "</td><td><a href='https://api.github.com/repos/hibernate/hibernate-orm/commits/" . $commitID . "'>Commit Link </a></td></tr>";
			    $counter ++;
			 }*/
			/* if(preg_match("/\b$xmlKey\b/", $keyInFile))
						{
						
							$commitLink = "https://api.github.com/repos/hibernate/hibernate-orm/commits/" . trim($commitID); //trim removes white space
							$jsonResults = getJsonFromUrl($commitLink);
							$printedResults = printJsonResults($jsonResults);
							
							insertFiles($xmlKey, $printedResults);
							$conn = connectToDB("localhost", "root" , "", "keysandfilechanges");
						    $sqlIssuesAndCommits = "INSERT INTO `issuesandcommits` (`Key`,`KeyLink`,`TimeEffort`,`CommitLink`) VALUES ('$xmlKey', '$link','$timespent','$commitLink')";
							$conn->query($sqlIssuesAndCommits);
							$conn->close();
							echo "<tr><td><a href='" . $link . "'>" . $xmlKey . "</a></td><td>" . $timespent . "</td><td><a href='" . $commitLink . "'>Commit Link </a></td><td><button>show or hide</button><p>". implode(" ",$printedResults) ."</p></td></tr>";
							$counter ++;
							
						}*/
						
						//Selection of files for a particular key e.g. HHH-XXX has file root/dir/path/file.java
						$conn = connectToDB("localhost", "root" , "", "keysandfilechanges");
						$sql = "SELECT Files FROM `keyandfile` WHERE `Key`='$xmlKey'";
						$result = $conn->query($sql);
						//make ready an array in order to insert the results in the array
						$filesChanged = array();
						//insert the results in array 
						if ($result->num_rows > 0) {
							// output data of each row
							while($row = $result->fetch_assoc()) {
							$filesChanged[$xmlKey][] = $row["Files"];
							
							}
						}
					
						
						
						$sql2 = "SELECT * FROM `issuesandcommits` WHERE `Key`='$xmlKey'";
						$result2 = $conn->query($sql2);
						if ($result2->num_rows > 0) {
							// output data of each row
							while($row = $result2->fetch_assoc()) {
							//check if the key from the sql2 results exist in the array of fileschanged	
							if(array_key_exists($xmlKey, $filesChanged)){
							$numberOfExtensions = countFileExtension($filesChanged[$xmlKey]);
							$heterogeneity = calculateHeterogeneity($numberOfExtensions, $filesChanged, $xmlKey);
							//get results of additions and deletions from the link which returns a JSON results
						//	$jsonResults = getJsonFromUrl($row['CommitLink']);
							//$additions = $jsonResults->stats->additions;
							//$deletions = $jsonResults->stats->deletions;
							//$totAddDel = $jsonResults->stats->total;
							//SQL insert commands, which serve as one time insert in the dataset tables, after the insert the commands should be commented
							  //$sql_insert = "INSERT INTO `dataset_heterogeneity` (`ProjectName`,`Key`,`TimeEffort`,`Heterogeneity`) VALUES ('Hibernate-ORM', '" . $row['Key'] . "', '$timespent', $heterogeneity)";
							  //$sql_insert = "INSERT INTO `dataset_numberoffileextensions_and_timespent` (`Project`,`NumberOfFileExtension`,`TimeSpent`) VALUES ('Hibernate-ORM', '" . sizeof($numberOfExtensions) . "', '$timespent')";
							  //$sql_insert = "INSERT INTO `dataset_nroffiles_timespent_extensionnr` (`ProjectName`,`NumberOfFiles`,`NumberOfExtensions`,`TimeSpent`) VALUES ('Hibernate-ORM', '" . sizeof($filesChanged[$xmlKey]) . "','" . sizeof($numberOfExtensions) . "', '$timespent')";
							  //$sql_insert = "INSERT INTO `dataset_component_longestprefix` (`ProjectName`,`longestPrefix`,`TimeSpent`,`NrOfFiles`) VALUES ('Hibernate-ORM', '" . longestCommonPrefix($filesChanged[$xmlKey]) . "','$timespent', '" . sizeof($filesChanged[$xmlKey]) . "')";
							  //$sql_insert = "INSERT INTO `dataset_addition_deletion` (`ProjectName`,`Key`,`TimeSpent`,`Additions`,`Deletions`,`TotalAddDel`,`NrOfFiles`,`NrOfExtensions`) VALUES ('Hibernate', '$xmlKey', '$timespent', '$additions', '$deletions', '$totAddDel', '" . sizeof($filesChanged[$xmlKey]) . "', '" . sizeof($numberOfExtensions) . "')";
								$sql_insert = "UPDATE `proxys` SET `LonegstCommonPrefix` = " . longestCommonPrefix($filesChanged[$xmlKey]) . " WHERE `Key` = '" . $row['Key'] . "'";
							    $conn->query($sql_insert);
							  //print the desired results in the web browser. This is helpful to see the results upon changes

							//saveFilenameAndLink($jsonResults, "HibernateFiles");
							echo "<tr><td><a href='" . $row['KeyLink'] . "'>" . $row['Key'] . "</a></td><td>" . $row['TimeEffort'] . "</td><td><a href='" . $row['CommitLink'] . "'> " . $row['CommitLink'] . " </a></td><td><button>show or hide</button><p>" . implode(" <br />",$filesChanged[$xmlKey]) . "</p></td><td>". sizeof($filesChanged[$xmlKey]) ."</td><td>" . http_build_query($numberOfExtensions, ' ', '<br />') . "</td><td>". $heterogeneity . "</td><td>" . longestCommonPrefix($filesChanged[$xmlKey]) . " </td></tr>";
							$counter ++;

							}
							}
						}
						$conn->close(); 
			
		//}
}
echo " <b> Resutls:" . $counter . "</b> <BR />";







?>

</table>
</body>
</html>