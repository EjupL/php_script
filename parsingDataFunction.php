<?php
//get the JSON data from the given URL code. This function makes a CURL request in order do get the data
function getJsonFromUrl($url)
{
	$agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
	$username = "write Your Git Hub Usernamte Here";
	$password = "write Your Git Hub Password Here";
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_URL,$url);
	curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
	curl_setopt($curl, CURLOPT_USERAGENT, $agent);
	//execute the curl 
	$json_response = curl_exec($curl);
	curl_close($curl);
	//return results
	return json_decode($json_response);
}
function getResultsFromUrl($url)
{
	$agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
	
	$username = "write Your Git Hub Usernamte Here";
	$password = "write Your Git Hub Password Here";
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($curl, CURLOPT_URL,$url);
	curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
	curl_setopt($curl, CURLOPT_USERAGENT, $agent);
	//execute the curl 
	$response = curl_exec($curl);
	curl_close($curl);
	//return results
	return $response;
}

function saveResultToFile($data, $projectName, $fileName, $extension, $filePaths)
{
	$path_parts = pathinfo($filePaths);
	
	if($extension=="java")
	{
	
	$pathDirectory = "" . $projectName . "\ " . $path_parts['dirname'];
	if (!file_exists($pathDirectory)) {
    mkdir($pathDirectory, 0777, true);
	}	
	
	$fp = fopen("" . $projectName . "\ " . $path_parts['dirname'] . "\ " . $fileName . "." . $extension ."", 'w');
	fwrite($fp, $data);
	fclose($fp);
	file_put_contents("HibernateFilePaths.txt", trim($filePaths . "\n"), FILE_APPEND);
	file_put_contents("HibernateFilePaths.txt", PHP_EOL, FILE_APPEND);
	}
}
//Get the JSON results and add to array. In order to be able to print the results.
function printJsonResults($results)
{
	$array = array();
	$i = 0;
	
	foreach($results->files as $file)
		{
		   $array[$i] = "$file->filename";
		   $i++;
		}
	return $array;
}
//save filenames and links of the file
//results are from the link that returnet json results
//data is the results that are returned as the file content e.g. java, or xml
function saveFilenameAndLink($results, $projectName)
{
		
	foreach($results->files as $file)
		{
		   $path_parts = pathinfo($file->filename);
		   $rawURL = $file->raw_url;
		  // echo $path_parts['filename'] . "<br />" . $rawURL . "<br />";
		   //exec("curl " . $rawURL . " -o test.txt");
		   $fileData = getResultsFromUrl($rawURL);
		  // $fileData = null;
		   saveResultToFile($fileData, $projectName, $path_parts['filename'], $path_parts['extension'],$file->filename );
		  
		}
	return $path_parts ;
}
//split the text 
function multiexplode ($delimiters,$string) {
    
    $ready = str_replace($delimiters, $delimiters[0], $string);
    $launch = explode($delimiters[0], $ready);
    return  $launch;
}
//readTxtFileOfGithub is a function that takes the txt file results and divides them in two groups
//the sha code with link, e.g. api/github/project/commits/shacode
// and the message log, e.g. "fixing the bug HHH-333 for Hibernate"
//and puts the results in array where key 0 = shaCode, 1 = message log
function readTxtFileOfGithub($TxtFile){
	$file_handle = fopen($TxtFile, "rb");
while (!feof($file_handle) ) {
	$line_of_text = fgets($file_handle);
	$parts = explode('=', $line_of_text);
	$lines = multiexplode(array("%a"),$parts[0]);
	//print $lines[1] . "<BR>";
	//if($lines[0] == null)
	  // echo $lines[0] . "  -> " . $lines[1];
	   
	$array[$lines[1]] = $lines[0];
	}
fclose($file_handle);
return $array;
}
//countFileExtension is a function that counts the number of extension, e.g. .java, .jsp, .xml = 3 extensions
//takes as argument an array that contains set  of strings with filepath e.g. package/root/path/filename.java::STRING 
 function countFileExtension($arrayOfFiles)
{
	$extensions = array();
	foreach($arrayOfFiles as $key => $file)
	{
		$path_parts = pathinfo($file); // path info allows to get properties such as file name, extension, dirname, etc.
		if(array_key_exists('extension', $path_parts)) // make sure that the key extension exist in this array
		{
		if (!array_key_exists ($path_parts['extension'] , $extensions)) // check if the extension exist in array if not, create this key in the array
           {
		    $extensions[$path_parts['extension']] = 1;
		   }
          else
            {
			 $extensions[$path_parts['extension']] += 1;          //if the key already exits, just increase the number of extension
			}	
        }			
		}
		
	
	return $extensions;
} 

//calculateHeterogeneity us a function that gets a array:numberOfExtensiont, array::filesChanged, and String::xmlKey
//returns the division of numberOfExtensions and number of filesChanged
function calculateHeterogeneity($numberOfExtensions, $filesChanged, $xmlKey)
{
	
	return (sizeof($numberOfExtensions) / sizeof($filesChanged[$xmlKey])); 

}

//longestCommonPrefix is a function that takes an array of strings and finds the longestCommonPrefix
function longestCommonPrefix($string)
{
 if(sizeof($string) == 0)
  return 0;
  
  
  for($prefixLength = 0; $prefixLength < strlen($string[0]); $prefixLength++)
  {
    $char = $string[0]{$prefixLength};
	for($i = 0; $i < sizeof($string); $i++)
	{ 
	 if($prefixLength >= strlen($string[$i]) || $string[$i]{$prefixLength} != $char)
	  { //A mismatch has been found
	     return strlen(substr($string[$i], 0 , $prefixLength));
	  
	  }
	  
	}
	
  }

  return strlen($string[0]);
} 

/* $string = array( 0 => "hello njeri i mencur", 1 => "hello njeri i vertet", 2 => "hello njeri i vram");

$res = longestCommonPrefix($string);
echo $res; */

?>